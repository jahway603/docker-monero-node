# docker-monero-node

Simple way to run a Monero node over the TOR network with some basic monitoring tools packaged in.

Leverages [Prometheus](https://prometheus.io/docs/introduction/overview/), [Grafana](https://grafana.com/), and [ExcitableAardvark/monerod_exporter](https://github.com/ExcitableAardvark/monerod_exporter.git) on top of `monerod`.

Forked from [lalanza808/docker-monero-node](https://github.com/lalanza808/docker-monero-node) on GitHub when it took a stall in updates around September 24, 2021.

## Setup

The only requirements are [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/). Ensure those are installed on your system. There's an optional `Makefile` provided if you'd like to use that, just ensure `make` is installed. Type `make help` for more information about the make options.

```
# Clone and enter the repository
git clone https://codeberg.org/jahway603/docker-monero-node
cd docker-monero-node

# OPTIONAL: Setup Grafana password, blockchain storage location, or port overrides
cp env-example .env
vim .env

# Build containers
docker-compose build  # make build
```

The following ports will be bound for `monerod` by default, but you can override in `.env`:
- 18080   # p2p
- 18081   # restricted rpc
- 18082   # zmq
- 18083   # unrestricted rpc

You will want to open/allow ports 18080 and 18081 in your firewall for usage as a remote/public node (or whichever p2p and restricted ports you picked).

### Successfully tested on

| Linux Distro | Architecture |
|--------------|--------------|
| Ubuntu 20.04 | x86 64-bit   |
| Ubuntu 18.04 | x86 64-bit   |

Please fork and submit a PR to submit Linux distribution(s) you've successfully tested this on.

If you are interested in running a Monero node on ARM architecture (like a Raspberry Pi), then check out the [PiNode-XMR Project](https://github.com/monero-ecosystem/PiNode-XMR).

## Usage

It's fairly simple, use `docker-compose` to bring the containers up and down and look at logs.

```
# Run containers
docker-compose up -d  # make up

# Check all logs
docker-compose logs -f  # make logs

# Check monerod logs
docker-compose logs -f monerod
```

Navigate to http://localhost:3000 and log into Grafana. Find the `Daemon Stats` dashboard to get those sweet, sweet graphs.

If you've installed this on another system you will want to use [SSH tunnels](https://www.ssh.com/ssh/tunneling/example) (local forwarding) to reach Grafana:

```
ssh <VPS OR SERVER IP> -L 3000:localhost:3000
```

Then navigate to http://localhost:3000. Here is what the graph looks like:

![](static/monerod_grafana.png)

## Updating your Monero daemon

If your Monero daemon is out of date, then you can do the following to update your node:

1. Change to the directory where you cloned this repository. `cd docker-monero-node`
1. Stop your node with `make down` (or `make down-full` if you compiled from source).
1. Pull down the latest for this repository with `git pull`
1. Build a new docker image with the latest Monero daemon with `make build` (or `make build-full` if you compiled from source).
1. Then run the new docker with `make up` (or `make up-full` if you compiled from source).
1. Then check that your node is running the latest version available.
1. If this is not the latest, please either [file an issue here](https://codeberg.org/jahway603/docker-monero-node/issues) or fork it and contribute the update back here.

```
You type:
docker exec -it monerod bash 

You see:
root@730cdd0ccc6e:/data# monerod --version
Monero 'Oxygen Orion' (v0.17.3.0-release)
```

## Changing your Monero daemon to a Private node

By default this docker experience sets up a Public Monero node. There are many
who will only want to use this privately, so this is the process you follow to change it:

1. Change to the directory where you cloned this repository. `cd docker-monero-node`
1. Stop your node with `make down` (or `make down-full` if you compiled from source).
1. Open docker-compose.yaml (or docker-compose.full.yaml if you compiled from source).
1. Change the monerod line by deleting the `--public-node` option and replacing it with `--rpc-login=YOUR_username:YOUR_password` as seen in the [Monero RPC here](https://monerodocs.org/interacting/monerod-reference/).
1. Build a new docker image with `make build` (or `make build-full` if you compiled from source).
1. Then run the new docker with `make up` (or `make up-full` if you compiled from source).
1. Then test the node with your Monero wallet.

## License

[Copyright 2021-2025](COPYING) under [GPLv3 License](LICENSE)
upstream had [MIT License](https://raw.githubusercontent.com/lalanza808/docker-monero-node/master/LICENSE)
